fun main() {
    println("Hello, world!!!")
    halo()
}

fun halo() {
    val name = "Budi" // val => immutable
    println(name)

    var age = 32 // var => mutable
    println(age);
    age = 33
    println(age);
    
    var title: String? = null
    println(title?.length)
    
    val school: String = "" // explicit type
    println(school)

    val company = "" // implicit type
    println(company);
    
    println("My age is $age")
}

class Halo

object MyObject

data class Employee(
    val name: String
)

fun basic() {}

fun functionWithReturnValue(): String {
    return ""
}

fun returnWith(): String = ""

fun implicitReturn() = ""

fun functionWithParameters(age: Int = 23) {
    println(age)
}