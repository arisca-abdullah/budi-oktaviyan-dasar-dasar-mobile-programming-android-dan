package com.aris.fragmentlifecycle

import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity

class MainActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val frameLayout = findViewById<FrameLayout>(R.id.fl_main)
        supportFragmentManager.beginTransaction().replace(R.id.fl_main, MainFragment()).commit()
    }
}