package com.aris.activitylifecycle

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

class MainActivity: AppCompatActivity(){
    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onPause() {
        super.onPause()
        Log.v(TAG, "OnPause")
    }

    override fun onResume() {
        super.onResume()
        Log.v(TAG, "OnResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.v(TAG, "OnDestroy")
    }
}