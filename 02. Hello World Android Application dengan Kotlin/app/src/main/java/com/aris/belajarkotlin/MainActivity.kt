package com.aris.belajarkotlin

import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

class MainActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnToast = findViewById<Button>(R.id.btn_toast)
        btnToast.setOnClickListener {
            Toast.makeText(this, "Hello, World!", Toast.LENGTH_SHORT).show()
        }

        val rootLayout = findViewById<LinearLayout>(R.id.root_layout)
        val btnSnackbar = findViewById<Button>(R.id.btn_snackbar)
        btnSnackbar.setOnClickListener {
            Snackbar.make(rootLayout, "Hello, World!", Snackbar.LENGTH_SHORT).show()
        }
    }
}